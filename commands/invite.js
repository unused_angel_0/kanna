const { RichEmbed } = require('discord.js')
const talkedRecently = new Set();

exports.run = async (client, message, args, level) => {
 if (talkedRecently.has(msg.author.id)) {
   msg.channel.send('Wait 1 minute before getting typing this again. - ' + msg.author);
 } else {
   talkedRecently.add(msg.author.id);
   setTimeout(() => {
     talkedRecently.delete(msg.author.id);
   }, 60000);
  }
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: ""
};

exports.help = {
  name: "",
  category: "",
  description: "",
  usage: ""
};