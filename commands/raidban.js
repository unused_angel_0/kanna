exports.run = async (client, message, args, level) => {
  args.forEach(a => {
    banlist = ''
    if((`${a} `).match(`/\d{17,18}\s{1}/gm`)) {
      banlist += a
    } else {
      message.mentions.users.forEach(mentionned => {
        banlist += mentionned.id
      })
    }
  })
  banlist.forEach(to_ban => {
    let ban = message.guild.fetchMember(to_ban)
    banned += ban
    if(ban.bannable) ban.ban(7, `${message.author} - Raid`).then(console.log(`Banned ${member.displayName}`))
    .catch(console.error);
  })
  message.reply(``)
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "Moderator"
};

exports.help = {
  name: "raidban",
  category: "Moderation",
  description: "Bans a list of provided users/members.",
  usage: "ban [users|IDs|@users]"
};