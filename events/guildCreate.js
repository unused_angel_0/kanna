const { RichEmbed } = require('discord.js')

module.exports = (client, guild) => {
  client.logger.cmd(`[GUILD JOIN] ${guild.name} (${guild.id}) added the bot. Owner: ${guild.owner.user.tag} (${guild.owner.user.id})`);
  let embed = new RichEmbed()
    .setColor('green')
    .setTitle('<:greentick:426469753666666516> Joined a new guild!')
    .setTimestamp()
    .setThumbnail(guild.iconURL)
    .setDescription(`► ${guild.name} (${guild.id})`)
    .setFooter(client.guilds.size-1 + '►' + client.guilds.size)
  client.channels.get('462672574439096330').send(embed)
};
