const { RichEmbed } = require('discord.js')

module.exports = (client, guild) => {
  client.logger.cmd(`[GUILD LEAVE] ${guild.name} (${guild.id}) removed the bot.`);
  let embed = new RichEmbed()
    .setColor('red')
    .setTitle('<:redtick:426469754052804628> Left a guild!')
    .setTimestamp()
    .setThumbnail(guild.iconURL)
    .setDescription(`► ${guild.name} (${guild.id})`)
    .setFooter(client.guilds.size+1 + '►' + client.guilds.size)
  client.channels.get('462672574439096330').send(embed)
  if (client.settings.has(guild.id)) {
    client.settings.delete(guild.id);
  }
};
